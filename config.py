# Telegram tipbot variables
import os
bot_name = os.environ['BOT_NAME']
bot_token = os.environ['BOT_TOKEN']
encoding = "utf-8"
accounts_wallet = "pandacoin-cli"
staking_wallet_address = os.environ['STAKING_WALLET_ADDRESS']
staking_wallet_rpc_ip = os.environ['STAKING_WALLET_RPC_IP']
staking_wallet_rpc_port = os.environ['STAKING_WALLET_RPC_PORT']
staking_wallet_rpc_user = os.environ['STAKING_WALLET_RPC_USER']
staking_wallet_rpc_pw = os.environ['STAKING_WALLET_RPC_PW']
market_data_origin = "https://api.coingecko.com/api/v3/coins/pandacoin?localization=false&tickers=false&community_data=false&developer_data=false"
tipbot_datadir = os.environ['DATADIR']
dev_fund_address = os.environ['DEV_FUND_ADDRESS']
dev_fund_balance_api_url = os.environ['EXPLORER_BASE_URL'] + "/api.dws?q=getbalance&a=" + dev_fund_address
#dev_fund_tx = "http://live.reddcoin.com/api/txs/?address=" + dev_fund_address
android_wallet_url = os.environ['ANDROID_WALLET_URL']
walletpassphrase = ""
image_home = tipbot_datadir + "/image/"
qrcode_logo_img = image_home + "tipbot_qrcode_logo.png"
leaderboard_max_entries = 10
admin_list = ["amDOGE"]
staking_json_file = tipbot_datadir + "/staking_tx.json"
tippings_json_file = tipbot_datadir + "/tippings.json"
users_json_file = tipbot_datadir + "/users.json"
donors_json_file = tipbot_datadir + "/donors.json"
scheduler_active = True
